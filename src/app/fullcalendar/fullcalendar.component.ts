import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { OptionsInput, Calendar } from '@fullcalendar/core';
import faLocale from '@fullcalendar/core/locales/fa';
import esLocale from '@fullcalendar/core/locales/es';
import dayGridPlugin from '@fullcalendar/daygrid';
import listPlugin from '@fullcalendar/list';
import timegridPlugin from '@fullcalendar/timegrid';
import { FullCalendarComponent } from '@fullcalendar/angular';
import interactionPlugin from '@fullcalendar/interaction';




@Component({
  selector: 'app-fullcalendar',
  templateUrl: './fullcalendar.component.html',
  styleUrls: ['./fullcalendar.component.scss']
})
export class FullcalendarComponent implements OnInit, AfterViewInit {
  options: OptionsInput;
  plugins: OptionsInput;
  @ViewChild('fullcalendar') fullcalendar: FullCalendarComponent;

  constructor() { }

  ngOnInit() {
    this.plugins = {
      plugins: [dayGridPlugin, timegridPlugin, listPlugin, interactionPlugin],
      defaultView: 'timeGridWeek',
    }
  }

  ngAfterViewInit(): void {
    this.options = {
      header: {
        left: 'dayGridMonth,timeGridDay,timeGridWeek,list',
        center: 'title',
        right: 'next,prev,nextYear,prevYear today'
      },
      editable: true,
      timeZone: 'local',
      // defaultDate: '2018-06-01',
      height: 'parent',
      contentHeight: 600,
      aspectRatio: 2,
      defaultView: 'timeGridWeek',
      navLinks: true,
      weekNumbers: true,
      selectable: true,
      nowIndicator: true,
      events: [
        { // this object will be "parsed" into an Event Object
          title: 'event test 1', // a property!
          start: new Date, // a property!
          end: new Date // a property! ** see important note below about 'end' **
        }
      ]
      

    }
    this.fullcalendar.getApi().setOptions(this.options)
    this.fullcalendar.getApi().setOption('editable', true);
    this.fullcalendar.getApi().setOption('locale', faLocale);
    
    this.fullcalendar.getApi().on('dateClick', event => {
      console.log(event);
    });
  }

}
